# Galician translation of aptitude
# Copyright (C) 2005 Daniel Burrows <dburrows@debian.org>
# This file is distributed under the same license as the aptitude package.
# Jacobo Tarrío <jtarrio@debian.org>, 2005, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: aptitude\n"
"Report-Msgid-Bugs-To: dburrows@debian.org\n"
"POT-Creation-Date: 2014-02-22 00:06+0000\n"
"PO-Revision-Date: 2012-02-23 16:46+0800\n"
"Last-Translator: Jacobo Tarrío <jtarrio@debian.org>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"

#: src/cwidget/config/column_definition.cc:273
msgid "Bad format parameter"
msgstr "Parámetro de formato incorrecto"

#: src/cwidget/dialogs.cc:115 src/cwidget/dialogs.cc:128
#: src/cwidget/dialogs.cc:311 src/cwidget/dialogs.cc:351
msgid "Ok"
msgstr "Aceptar"

#: src/cwidget/dialogs.cc:201 src/cwidget/dialogs.cc:239
msgid "Yes"
msgstr "Si"

#: src/cwidget/dialogs.cc:202 src/cwidget/dialogs.cc:240
msgid "No"
msgstr "Non"

#: src/cwidget/dialogs.cc:352
msgid "Cancel"
msgstr "Cancelar"

#: src/cwidget/generic/threads/threads.cc:33
msgid "Not enough resources to create thread"
msgstr "Non hai recursos de abondo para crear o fío"

#: src/cwidget/toplevel.cc:158
#, c-format
msgid "Ouch!  Got SIGTERM, dying..\n"
msgstr "Recibiuse SIGTERM, a saír...\n"

#: src/cwidget/toplevel.cc:161
#, c-format
msgid "Ouch!  Got SIGSEGV, dying..\n"
msgstr "Recibiuse SIGSEVG, a saír...\n"

#: src/cwidget/toplevel.cc:164
#, c-format
msgid "Ouch!  Got SIGABRT, dying..\n"
msgstr "Recibiuse SIGABRT, a saír...\n"

#: src/cwidget/toplevel.cc:167
#, c-format
msgid "Ouch!  Got SIGQUIT, dying..\n"
msgstr "Recibiuse SIGQUIT, a saír...\n"

#: src/cwidget/toplevel.cc:957
msgid "yes_key"
msgstr "s"

#: src/cwidget/toplevel.cc:958
msgid "no_key"
msgstr "n"

#: src/cwidget/widgets/pager.cc:476
#, c-format
msgid ""
"Unable to load filename: the string %ls has no multibyte representation."
msgstr ""
"Non se puido cargar o ficheiro: a cadea %ls non ten unha representación "
"multibyte."

#: src/cwidget/widgets/tree.cc:1009
msgid "TOP LEVEL"
msgstr "NIVEL SUPERIOR"
